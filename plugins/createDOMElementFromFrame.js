import Slide from '~/components/Slide/Slide.vue'
import BLinkV from '~/components/Button/BLinkV.vue'
import BWin from '~/components/Button/BWin.vue'
import Slide2 from '~/components/Slide/Slide2.vue'
import inputText from '~/components/input/inputText.vue'
import inputEmail from '~/components/input/inputEmail.vue'
import inputPassword from '~/components/input/inputPassword.vue'
import inputTextarea from '~/components/input/inputTextarea.vue'
import layoutNav from '~/components/layoutElements/layoutNav.vue'
import buttonSave from '~/components/Button/buttonSave.vue'
import inputCheckbox from '~/components/input/inputCheckbox.vue'
import inputSelect from '~/components/input/inputSelect.vue'
import listData from '~/components/list/listData.vue'
import curl from '~/components/curl/curl.vue'
import axios from 'axios'
const MyPlugin = {
  install (Vue, options) {
    Vue.mixin({
      components: {
        'Slide': Slide,
        'BLinkV': BLinkV,
        'BWin': BWin,
        'inputText': inputText,
        'inputEmail': inputEmail,
        'inputPassword': inputPassword,
        'inputTextarea': inputTextarea,
        'Slide2': Slide2,
        'layoutNav': layoutNav,
        'buttonSave': buttonSave,
        'inputCheckbox': inputCheckbox,
        'inputSelect': inputSelect,
        'listData': listData,
        'curl': curl
      },
      data: function () {
        return {
          elements: [
            { type: 'Slide', 'level': '1', 'position': 'hidden' }],
          elementsSlide: []
        }
      },
      methods: {
        createDOMElementFromFrame: function (sqlResult) {
          this.elements = [{ type: 'Slide', 'level': '1', 'position': 'hidden' }]
          for (var j in sqlResult) {
            var functionCall = sqlResult[j]['FIELD1']
            console.log(functionCall)
            this[functionCall](sqlResult[j]['FIELD1'], sqlResult[j]['FIELD2'], sqlResult[j]['FIELD3'], sqlResult[j]['FIELD4'], sqlResult[j]['FIELD5'], sqlResult[j]['FIELD6'], sqlResult[j]['FIELD7'])
          }
          this.elements.shift()
          console.log(this.elements)
          var domElements = ''
          var i = 0
          while (i < this.elements.length) {
            if (this.elements[i].type === 'Slide') {
              if (i !== 0) {
                if (this.elements[i].type === 'Slide' && this.elements[i].level === this.elements[i - 1].level) {
                  domElements += '],'
                } else if (this.elements[i].type === 'Slide' && this.elements[i].level < this.elements[i - 1].level) {
                  var diff = this.elements[i - 1].level - this.elements[i].level + 1
                  console.log(diff)
                  for (j = 0; j <= diff - 1; j++) {
                    if (j === diff - 1) {
                      domElements += '],'
                    } else {
                      domElements += ']'
                    }
                  }
                }
              }
              domElements += '['
              domElements += JSON.stringify(this.elements[i])
              domElements += ','
            }
            if (this.elements[i].type !== 'Slide') {
              domElements += JSON.stringify(this.elements[i])
              try {
                if (this.elements[i + 1].type !== 'Slide') {
                  domElements += ','
                }
              } catch (e) {
              }
            }
            if (typeof this.elements[i + 1] === 'undefined') {
              diff = this.elements[i].level
              for (j = 0; j <= diff - 1; j++) {
                domElements += ']'
              }
            }
            i++
          }
          console.log(domElements)
          this.elementsSlide = JSON.parse(domElements)
          // this.create()
        },
        Slide: function (param1, param2, param3, param4, param5, param6, param7) {
          this.elements[this.elements.length] = {'type': param1, 'level': param2, 'name': param3, 'position': param4, 'backgroundColor': param5, 'height': param6, layout: param7}
        },
        Slide2: function (param1, param2, param3, param4, param5, param6, param7) {
          this.elements[this.elements.length] = {'type': param1, 'level': param2, 'name': param3, 'position': param4, 'backgroundColor': param5, 'height': param6, layout: param7}
        },
        BLinkV: function (param1, param2, param3, param4, param5, param6, param7) {
          this.elements[this.elements.length] = {'type': param1, 'level': param2, 'name': param3, 'position': param4, 'backgroundColor': param5, 'height': param6, layout: param7}
        },
        BWin: function (param1, param2, param3, param4, param5, param6, param7) {
          this.elements[this.elements.length] = {'type': param1, 'level': param2, 'name': param3, 'position': param4, 'backgroundColor': param5, 'height': param6, 'layout': param7}
        },
        inputText: function (param1, param2, param3, param4, param5, param6, param7) {
          this.elements[this.elements.length] = {'type': param1, 'level': param2, 'name': param3, 'position': param4, 'backgroundColor': param5, 'height': param6, 'layout': param7}
        },
        inputEmail: function (param1, param2, param3, param4, param5, param6, param7) {
          this.elements[this.elements.length] = {'type': param1, 'level': param2, 'name': param3, 'position': param4, 'backgroundColor': param5, 'height': param6, 'layout': param7}
        },
        inputPassword: function (param1, param2, param3, param4, param5, param6, param7) {
          this.elements[this.elements.length] = {'type': param1, 'level': param2, 'name': param3, 'position': param4, 'backgroundColor': param5, 'height': param6, 'layout': param7}
        },
        inputTextarea: function (param1, param2, param3, param4, param5, param6, param7) {
          this.elements[this.elements.length] = {'type': param1, 'level': param2, 'name': param3, 'position': param4, 'backgroundColor': param5, 'height': param6, 'layout': param7}
        },
        layoutNav: function (param1, param2, param3, param4, param5, param6, param7) {
          this.elements[this.elements.length] = {'type': param1, 'level': param2, 'name': param3, 'position': param4, 'backgroundColor': param5, 'height': param6, 'layout': param7}
        },
        buttonSave: function (param1, param2, param3, param4, param5, param6, param7) {
          this.elements[this.elements.length] = {'type': param1, 'level': param2, 'name': param3, 'position': param4, 'backgroundColor': param5, 'height': param6, 'layout': param7}
        },
        inputCheckbox: function (param1, param2, param3, param4, param5, param6, param7) {
          this.elements[this.elements.length] = {'type': param1, 'level': param2, 'name': param3, 'position': param4, 'backgroundColor': param5, 'height': param6, 'layout': param7}
        },
        inputSelect: function (param1, param2, param3, param4, param5, param6, param7) {
          this.elements[this.elements.length] = {'type': param1, 'level': param2, 'name': param3, 'position': param4, 'backgroundColor': param5, 'height': param6, 'layout': param7}
        },
        listData: function (param1, param2, param3, param4, param5, param6, param7) {
          this.elements[this.elements.length] = {'type': param1, 'level': param2, 'name': param3, 'position': param4, 'backgroundColor': param5, 'height': param6, 'layout': param7}
        },
        curl: function (param1, param2, param3, param4, param5, param6, param7) {
          this.elements[this.elements.length] = {'type': param1, 'level': param2, 'name': param3, 'position': param4, 'backgroundColor': param5, 'height': param6, 'layout': param7}
        }
      }
    })
  }
}

export default MyPlugin
