import Vue from 'vue'
import * as VueGoogleMaps from '~/node_modules/vue2-google-maps/src/main'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCNHVD7f-CK64zW_B_1_let8V_tKRzv0cU',
    libraries: 'places,drawing'
  }
})
