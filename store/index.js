const state = () => ({
  storElements: []
})
const mutations = {
  storElements (state, data) {
    state.storElements = data
  }
}
const actions = {
  storElements ({commit}, submit) {
    commit('storElements', submit)
  }
}
const getters = {
  storElements: state => state.storElements
}
export {state, mutations, actions, getters}
