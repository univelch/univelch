# univelch FrontOffice

## Project Setup 
#### Initialize project

``` bash
# Clone this repo
$ git clone https://LamGui@bitbucket.org/medicateam/univelch-front.git
# install dependencies
$ npm install # Or yarn install
``` 

#### Build for dev without SSR
``` 
# serve with hot reload at localhost:3000 without SSR
$ npm run dev
``` 

#### Build for dev with SSR
``` 
# serve with hot reload at localhost:3000 with SSR
$ npm run ssr
``` 

#### Build for production and launch server
``` 
$ npm run build
$ npm start
``` 

## Tools
 - ESLinter   `$ npm run lint`

## Git flow
 - Prefix your commit with JIRA issue number
    
    `1234 - Introduce new awesome feature`
  - Commit should contain action verb and describe the feature
    

## Resources 

[Nuxt.js docs](https://github.com/nuxt/nuxt.js)
