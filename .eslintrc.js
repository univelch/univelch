module.exports = {
  root: true,
  parser: 'babel-eslint',
  env: {
    browser: true,
    node: true
  },
  extends: 'standard',
  // required to lint *.vue files
  plugins: [
    'html'
  ],
  // add your custom rules here
  rules: {
    'standard/computed-property-even-spacing': "off",
    'no-unused-vars': "off",
    'no-array-constructor': "off"
  },
  globals: {}
}
